FROM gcc:latest

COPY . /usr/src/c_test

WORKDIR /usr/src/c_test

RUN make all

CMD [ "./app" ]