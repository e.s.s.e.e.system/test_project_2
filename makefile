all:
	gcc app.c -g -lm -o app

go:
	./app

clean:
	rm -f app

image:
	docker build . -t recapitulate

cont:
	docker run --rm -it recapitulate

inf:
	docker images
	docker ps

del:
	docker rmi recapitulate

del_img:
	docker rmi $$(docker images)